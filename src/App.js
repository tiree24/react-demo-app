import React from 'react';
import {Route, Switch} from 'react-router-dom';
import './App.css';
import Welcome from './components/welcome/Welcome';
import Clock from './components/clock/Clock';
import Contact from './components/contact/Contact';
import Navigation from './components/navigation/Navigation';
import FourOFour from './components/fourofour/FourOFour';



function App() {
  return (
    <div className="App">
      <Navigation />
      <Switch>
      <Route exact 
      path="/"  
  render={(props) => <Welcome {...props} name="Tiree"/>} /> 
  <Route exact path="/welcome/:name" component={Welcome} />
  <Route exact path="/welcome" render={(props) => <Welcome {...props} name="Tiree"/>} /> 
 <Route  path="/clock" component={Clock} />
 <Route path="/contact" component={Contact} />
 <Route component={FourOFour} />
 </Switch>
    </div>
  );
}

export default App;
